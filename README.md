Below are the steps to run the app.

install python 3.8

clone the code from github using git cline "link"

create virtual environment using below command.

python3 -m venv envname

activate virtual environment using below command.

source envname/bin/activate

install requirements using below command.

pip install -r requirements.txt

then run the code from main.py file.
