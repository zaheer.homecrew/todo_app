from typing import Optional

from fastapi import APIRouter, Depends, Form
from sqlalchemy.orm import Session

from api import api
from configurations.db import SessionLocal

router = APIRouter(prefix="/todo_app")


def get_session():
    """
    Gets current session of database to execute queries
    :return:
    """
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()


@router.get('/items')
async def get_items(session: Session = Depends(get_session)):
    items = api.get_items(session=session)
    return items


@router.post('/add_item')
async def add_item(
        name: str = Form(...), description: Optional[str] = Form(...), session: Session = Depends(get_session)
):
    return api.add_items(title=name, description=description, session=session)


@router.post('/update_item')
async def update_item(
    item_id: int = Form(...), update_title: Optional[str] = Form(...), updated_description: Optional[str] = Form(...),
    status: Optional[str] = Form(...), session=Depends(get_session)
):
    return api.update_item(
        item_id=item_id, updated_title=update_title,
        status=status, updated_description=updated_description, session=session
    )


@router.post('/delete_item')
async def delete_item(item_id: int = Form(...), session: Session = Depends(get_session)):
    return api.delete_item(item_id=item_id, session=session)
