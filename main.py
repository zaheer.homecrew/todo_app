import uvicorn
from fastapi import FastAPI

from routing import routing

if __name__ == "__main__":
    app = FastAPI()
    app.include_router(routing.router)
    uvicorn.run(app)
