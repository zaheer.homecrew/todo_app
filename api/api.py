from typing import List

from sqlalchemy.orm import Session

from configurations.constants import *
from models import Items


def get_items(session: Session) -> List[Items]:
    """
    Get Items on TODO List
    :param session: Session
    """
    try:
        items = session.query(Items).all()
        if not items:
            return NO_ITEM_FOUND
    except:
        return NO_ITEM_FOUND
    return items


def add_items(title, description, session: Session) -> List[Items]:
    """
    Add items in todo list
    :param title: Title
    :param description: Description
    :param session: Session
    """
    try:
        task_data = {
            'name': title,
            'status': '1',
            'description': description
        }
        task_data = Items(**task_data)
        session.add(task_data)
        session.commit()
        session.refresh(task_data)
    except:
        return TRY_AGAIN
    return RECORD_ADDED


def update_item(item_id, updated_title, updated_description, status, session: Session) -> List[Items]:
    """
    Updated items in todo list
    :param int item_id: Item Id
    :param str updated_title: Updated Title
    :param str updated_description:Updated Description
    :param str status: Updated Status
    :param session: Session
    """
    if not (updated_description or updated_title or status):
        return NOTHING_TO_UPDATE
    try:
        if item_id:
            item = session.query(Items).filter(Items.id == item_id).first()
            if item:
                session.query(Items).filter(Items.id == item_id).update({
                    Items.name: updated_title if updated_title else item.name,
                    Items.description: updated_description if updated_description else item.description,
                    Items.status: STATUSES.get(status.lower(), 1) if status else item.status
                }, synchronize_session=False)
                session.commit()
            return RECORD_UPDATED
    except:
        return TRY_AGAIN
    return NO_RECORD_FOUND


def delete_item(item_id, session: Session ) -> List[Items]:
    """
    Delete Items in TODO List
    :param int item_id: Item Id
    :param session: Session
    """
    try:
        session.query(Items).filter(Items.id == item_id).delete()
        session.commit()
    except:
        return NO_RECORD_FOUND
    return RECORD_DELETED
