import datetime

from sqlalchemy import Column, Integer, TIMESTAMP, VARCHAR
from sqlalchemy.dialects.mssql import TINYINT

from configurations.db import Base


class Items(Base):
    __tablename__ = "items"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(VARCHAR(255), nullable=True)
    description = Column(VARCHAR(555), nullable=True)
    status = Column(TINYINT, nullable=False, index=True, default=0)
    created_at = Column(TIMESTAMP, default=datetime.datetime.now())
    updated_at = Column(TIMESTAMP, default=datetime.datetime.now())
